package xin.shaozeming.base.dao;

import xin.shaozeming.base.po.RolePO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-08-22
 */
public interface RoleDao extends BaseMapper<RolePO> {

}
