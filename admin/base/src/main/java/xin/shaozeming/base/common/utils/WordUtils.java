package xin.shaozeming.base.common.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: 邵泽铭
 * @date: 2019/5/10
 * @description:
 **/
public class WordUtils {
    public static String trans(String str){

        List record =new ArrayList();
        for(int i=0;i<str.length();i++)
        {
            char tmp =str.charAt(i);

            if((tmp<='Z')&&(tmp>='A'))
            {
                record.add(i);//记录每个大写字母的位置
            }

        }
        record.remove(0);//第一个不需加下划线

        str= str.toLowerCase();
        char[] charofstr = str.toCharArray();
        String[] t =new String[record.size()];
        for(int i=0;i<record.size();i++)
        {
            t[i]="_"+charofstr[(int)record.get(i)];//加“_”
        }
        String result ="";
        int flag=0;
        for(int i=0;i<str.length();i++)
        {
            if((flag<record.size())&&(i==(int)record.get(flag))){
                result+=t[flag];
                flag++;
            }
            else
                result+=charofstr[i];
        }

        return result;
    }
    public static String tansGet(String filed) {
       String first= filed.substring(0,1 );
       return  "get"+first.toUpperCase()+filed.substring(1,filed.length() );

    }

}
