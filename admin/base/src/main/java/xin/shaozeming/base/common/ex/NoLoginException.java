package xin.shaozeming.base.common.ex;

/**
 * @author: 邵泽铭
 * @date: 2019/6/21
 * @description:
 **/
public class NoLoginException extends  RuntimeException {
    private static final long serialVersionUID = -7230511035637305292L;

    public NoLoginException(){
        super();
    }

    public NoLoginException(String msg){
        super(msg);
    }
}
