package xin.shaozeming.base.common.ex;

/**
 * @author: 邵泽铭
 * @date: 2019/4/8
 * @description:
 **/
public class NetRunException extends  RuntimeException {
    public NetRunException(){
        super();
    }

     public NetRunException(String msg){
         super(msg);
     }
}
