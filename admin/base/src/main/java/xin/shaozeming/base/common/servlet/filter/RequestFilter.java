package xin.shaozeming.base.common.servlet.filter;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import xin.shaozeming.base.common.servlet.ParameterRequestWrapper;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: 邵泽铭
 * @date: 2019/9/10
 * @description:
 **/
@Log4j2
@Component
public class RequestFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest servletRequest=((HttpServletRequest)request);
        log.info("发起请求:url {} ,参数:{}",servletRequest.getRequestURI(),JSONObject.toJSONString(request.getParameterMap()));

        Map<String, String[]> params = new HashMap<String, String[]>(
                request.getParameterMap());
        request = new ParameterRequestWrapper((HttpServletRequest) request, params);

        chain.doFilter(request,response);
    }
}
