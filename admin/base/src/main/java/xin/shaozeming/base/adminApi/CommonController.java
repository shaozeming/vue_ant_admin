package xin.shaozeming.base.adminApi;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import xin.shaozeming.base.common.Enum.base.State;
import xin.shaozeming.base.common.Response;
import xin.shaozeming.base.common.aop.annotation.ParamsDecrypted;
import xin.shaozeming.base.common.aop.annotation.SysLogger;
import xin.shaozeming.base.common.aop.annotation.TokenCheck;
import xin.shaozeming.base.common.utils.AliUploadUtils;
import xin.shaozeming.base.common.utils.ObjConverter;
import xin.shaozeming.base.common.utils.TokenUtils;
import xin.shaozeming.base.config.Constant;
import xin.shaozeming.base.po.UserPO;

import xin.shaozeming.base.service.PermissionService;
import xin.shaozeming.base.service.UserService;
import xin.shaozeming.base.vo.PermissionVO;
import xin.shaozeming.base.vo.UserInfoVO;
import xin.shaozeming.base.vo.UserVO;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author: 邵泽铭
 * @date: 2019/4/8
 * @description:
 **/
@SysLogger
@RestController
@RequestMapping("common")
public class CommonController {
    @Resource
    private UserService userService;
    @Resource
    private PermissionService permissionService;

    @SysLogger(log="用户登录",user = true,userPosition = 2)
    @ParamsDecrypted
    @ApiOperation(value = "登陆", notes = "登陆", response = Response.class)
    @PostMapping("login")
    public Response login(HttpServletRequest request,
                          @ApiParam(value = "username", name = "username") String username,
                          @ApiParam(value = "password", name = "password") String password) {
        UserVO userVO = userService.login(username, password);
        return new Response<>(userVO);
    }

    @SysLogger(log="用户登录")
    @ParamsDecrypted
    @ApiOperation(value = "登陆", notes = "登陆", response = Response.class)
    @PostMapping("loginByToken")
    public Response loginByToken(HttpServletRequest request) {
        UserPO userPO = userService.checkToken(TokenUtils.getToken(request));
        if (userPO == null) {
            return new Response(State.RES_NOUSERORPASS.getCode());
        } else {
            UserVO userVO = ObjConverter.convert(userPO, UserVO.class);
            return new Response<>(userVO);
        }
    }


    @ParamsDecrypted
    @ApiOperation(value = "查询用户信息", notes = "查询用户信息", response = Response.class)
    @PostMapping("selectUserInfo")
    public Response selectUserInfo(HttpServletRequest request) {

        UserPO userPO = userService.checkToken(TokenUtils.getToken(request));
        if (!StringUtils.isEmpty(userPO)) {

            UserInfoVO userInfoVO=new UserInfoVO();
            userInfoVO.setPermissions( permissionService.selectMenuListByUser(userPO.getId()));
            userInfoVO.setUser(ObjConverter.convert(userPO, UserVO.class));
            return new Response<>(userInfoVO);
        } else {
            return new Response<>(State.RES_NOLOGIN.getCode());
        }
    }


    @ApiOperation(value = "文件上传", notes = "文件上传", response = Response.class)
    @TokenCheck
    @PostMapping("upload")
    public Response upload(
            HttpServletRequest request,
            @ApiParam(value = "token", name = "token") String token,
            @ApiParam(value = "file", name = "文件") MultipartFile file) {

        Map<String, Object> map = AliUploadUtils.upload(file, Constant.path_org_logo);
        return new Response<>(map);
    }


}
