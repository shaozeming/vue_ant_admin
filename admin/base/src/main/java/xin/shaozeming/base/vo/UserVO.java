package xin.shaozeming.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author: 邵泽铭
 * @date: 2019/4/8
 * @description:
 **/
@Data
public class UserVO {
    @ApiModelProperty(value = "用户code")
    private String code;


    @ApiModelProperty(value = "短信验证码")
    private String smsCode;

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty(value = "token过期时间")
    private Date tokenExpire;

    @ApiModelProperty(value = "用户姓名")
    private String nickName;

    @ApiModelProperty(value = "0 普通账号 1 手机号 ")
    private Integer accountType;

    @ApiModelProperty(value = "用户手机号")
    private String mobile;

    @ApiModelProperty(value = "0 使用 1 删除 2 停用")
    private Integer state;

    @ApiModelProperty(value = "上一次登陆时间")
    private Date lastLoginTime;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date modifiedTime;
}
