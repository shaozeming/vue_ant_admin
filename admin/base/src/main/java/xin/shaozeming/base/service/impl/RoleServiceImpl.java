package xin.shaozeming.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.transaction.annotation.Transactional;
import xin.shaozeming.base.po.RolePO;
import xin.shaozeming.base.dao.RoleDao;
import xin.shaozeming.base.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-08-22
 */
@Transactional
@Service
public class RoleServiceImpl extends ServiceImpl<RoleDao, RolePO> implements RoleService {


    @Resource
    private  RoleDao roleDao;
    /**
     * 查询其他角色
     *
     * @param list
     * @return
     */
    @Override
    public List<RolePO> selectRoleNotIn(List<Integer> list) {
        QueryWrapper<RolePO> queryWrapper=new QueryWrapper<>();
        if(list.size()>0){
            queryWrapper.notIn("id",list);
        }

        return roleDao.selectList(queryWrapper);
    }
}
