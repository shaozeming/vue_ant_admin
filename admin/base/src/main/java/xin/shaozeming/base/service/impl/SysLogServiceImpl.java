package xin.shaozeming.base.service.impl;

import org.springframework.transaction.annotation.Transactional;
import xin.shaozeming.base.po.SysLogPO;
import xin.shaozeming.base.dao.SysLogDao;
import xin.shaozeming.base.service.SysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-16
 */
@Transactional
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogDao, SysLogPO> implements SysLogService {

}
