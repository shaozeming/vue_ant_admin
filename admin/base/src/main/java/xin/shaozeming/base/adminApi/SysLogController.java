package xin.shaozeming.base.adminApi;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import xin.shaozeming.base.common.BaseController;
import xin.shaozeming.base.common.aop.annotation.Permission;
import xin.shaozeming.base.common.aop.annotation.SysLogger;
import xin.shaozeming.base.po.SysLogPO;
import xin.shaozeming.base.service.SysLogService;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-16
 */
@SysLogger(group = "操作日志")
@Permission(code = "syslog:list")
@RestController
@RequestMapping("adminApi/syslog")
public class SysLogController extends BaseController<SysLogService,SysLogPO> {

}
