package xin.shaozeming.base.common.Enum.base;

/**
 * Created by nixianjing on 17/2/27.
 */
public enum CommonEnum {
    //请求头的key
    REQ_HEAD_IMEI("h1", "设备编号"),//设备编号
    REQ_HEAD_MANUFACTURER("h2", "设备生产商"), //信息来源，分android,ios,wx,
    REQ_HEAD_SOURCE("h3", "信息来源"), //信息来源，分android,ios,wx,
    REQ_HEAD_OS_VERSION("h4", "设备操作系统版本"),//设备操作系统版本
    REQ_HEAD_APK_VERSION("h5", "版本号"),//版本号,如：2.2.2
    REQ_HEAD_TIME_STAMP("h6", "时间戳"),//时间戳,格式：yyyyMMddHHmmss如：20150409160033
    REQ_HEAD_LONGITUDE("h7", "经度"),//经度
    REQ_HEAD_LATITUDE("h8", "纬度");//纬度

    private String key;
    private String value;

    CommonEnum(String key, String value){
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
