package xin.shaozeming.base.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_quartz")
@ApiModel(value="QuartzPO对象", description="")
public class QuartzPO extends Model<QuartzPO> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "任务名")
    private String quartzName;

    @ApiModelProperty(value = "cron表达式")
    private String cron;

    @ApiModelProperty(value = "参数")
    private String param;

    @ApiModelProperty(value = "描述")
    private String quartzDesc;

    @ApiModelProperty(value = "0 启动 1暂停")
    private Integer quartzState;

    private Date createdTime;

    private Date modifiedTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
