
package xin.shaozeming.base.adminApi;



import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import xin.shaozeming.base.common.BaseController;
import xin.shaozeming.base.common.Enum.base.LoggerType;
import xin.shaozeming.base.common.Enum.base.State;
import xin.shaozeming.base.common.Response;
import xin.shaozeming.base.common.aop.annotation.ParamsDecrypted;
import xin.shaozeming.base.common.aop.annotation.Permission;
import xin.shaozeming.base.common.aop.annotation.SysLogger;
import xin.shaozeming.base.common.aop.annotation.TokenCheck;
import xin.shaozeming.base.po.RolePO;
import xin.shaozeming.base.service.PermissionService;
import xin.shaozeming.base.service.RolePermissionService;
import xin.shaozeming.base.service.RoleService;
import xin.shaozeming.base.service.UserRoleService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-08-22
 */
@SysLogger(group = "角色")
@Permission(code = "role:list")
@RestController
@RequestMapping("adminApi/role")
public class RoleController extends BaseController<RoleService,RolePO> {
    @Resource
    private PermissionService permissionService;
    @Resource
    private RolePermissionService rolePermissionService;
    @Resource
    private RoleService roleService;

    @SysLogger(name = "更新权限",type = LoggerType.UPDATE)
    @ParamsDecrypted
    @TokenCheck
    @ApiOperation(value = "保存权限", notes = "保存权限", response = Response.class)
    @PostMapping("saveRoleAuth")
    public Response saveRoleAuth(HttpServletRequest request,
                                 @ApiParam(name = "id" ,value = "id") String id ,
                                 @ApiParam(name = "ids" ,value = "ids") String ids ){

        if(StringUtils.isEmpty(ids)){
            return new Response<>(State.RES_PARAMERROR.getCode());
        }

        if(rolePermissionService.updateRolePermissionByRole(new Integer(id),ids)){
            return new Response<>(State.RES_SUCCES.getCode() );
        }else {
            return new Response<>(State.RES_ERROR.getCode() );
        }

    }


    @Permission(code = "admin:list")
    @TokenCheck
    @ParamsDecrypted
    @ApiOperation(value = "查询所有", notes = "查询所有", response = Response.class)
    @PostMapping("selectAll")
    public Response selectAll(HttpServletRequest request) {
        return new Response<>(roleService.list());
    }



    @ParamsDecrypted
    @ApiOperation(value = "查找目录权限列表", notes = "查找目录权限列表", response = Response.class)
    @PostMapping("selectAuthority")
    public Response selectAuthority(HttpServletRequest request,
                                    @ApiParam(name = "id" ,value = "角色id") String id ){
        Map<String,List> map=new HashMap<>();
        map.put("all",permissionService.selectMenuList());
        map.put("role",permissionService.selectPermissionByRole(new Integer(id)));
        return new Response<>(map);
    }


}
