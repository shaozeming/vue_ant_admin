package xin.shaozeming.base.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import xin.shaozeming.base.common.Enum.base.LoggerType;

/**
 * <p>
 * 
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_sys_log")
@ApiModel(value="SysLogPO对象", description="")
public class SysLogPO extends Model<SysLogPO> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "操作用户")
    private String username;

    @ApiModelProperty(value = "ip地址")
    private String ip;

    @ApiModelProperty(value = "操作内容")
    private String content;

    @ApiModelProperty(value = "参数")
    private String params;

    @ApiModelProperty(value = "0 查询 1 添加 2 删除 3 修改 4导入 5 导出")
    private LoggerType type;

    private Date createdTime;

    private Date modifiedTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
