package xin.shaozeming.base.service;

import xin.shaozeming.base.po.RolePermissionPO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-12
 */
public interface RolePermissionService extends IService<RolePermissionPO> {


    /**
     * 更新角色权限
     * @return
     */
    boolean updateRolePermissionByRole(Integer roleId, String ids);
}
