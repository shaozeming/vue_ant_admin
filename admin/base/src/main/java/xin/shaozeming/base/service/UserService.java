package xin.shaozeming.base.service;

import org.springframework.transaction.annotation.Transactional;
import xin.shaozeming.base.po.UserPO;
import com.baomidou.mybatisplus.extension.service.IService;
import xin.shaozeming.base.vo.UserVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-08-22
 */
public interface UserService extends IService<UserPO> {

    /**
     * 检测token是否失效
     * @param token
     * @return
     */
    UserPO checkToken(String token);


    /**
     * 登录
     * @param userName
     * @return
     */
    UserVO login(String userName, String password);

    /**
     * 修改密码
     * @param token
     * @return
     */
    boolean updatePassword(String token, String newPassword, String oldPassword);

    /**
     * 保存用户
     * @param userPO,roleIds
     * @return
     */
    boolean saveUser(UserPO userPO, String roleIds);
}
