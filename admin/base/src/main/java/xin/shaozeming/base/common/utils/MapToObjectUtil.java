package xin.shaozeming.base.common.utils;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.Map;
/**
 * Map转Object工具类
 * Created by nixianjing on 17/5/4.
 */
public class MapToObjectUtil {


    public static Object transfer(Map<String,Object> map,Object obj){
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : propertyDescriptors) {
                String key = property.getName();
                if (map.containsKey(key)) {
                    Object value = map.get(key);
                    // 得到property对应的setter方法
                    Method setter = property.getWriteMethod();
                    try {
                        setter.invoke(obj, value);
                    } catch (IllegalArgumentException ex) {
                        System.out.println("what the fuck?");
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("what the fuck?");
        }
        return obj;
    }
}
