﻿@echo off
setlocal enabledelayedexpansion

for /f "eol=* tokens=*" %%i in ('netstat -an -o ^| findstr "0:9091"') do (
set a=%%i
set a=!a:~69,10!
taskkill /F /pid !a!
)

echo startgasmeter......
START javaw -jar -server -Xms400m -Xmx400m -Xmn120M   -Xss256k -XX:PermSize=32m -XX:MaxPermSize=32m  -XX:+PrintGCDetails  -XX:+PrintGCTimeStamps -XX:+HeapDumpOnOutOfMemoryError  C:\Users\Administrator\Desktop\admin\admin.jar  --spring.profiles.active=dev
echo suceess...
