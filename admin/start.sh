#!/bin/bash
echo Stop Application ..........
PID=$(ps -ef | grep admin.jar | grep -v grep | awk '{ print $2 }')
if [ -z "$PID" ]
then
    echo Application is already stopped
else
    echo kill $PID
    kill $PID
fi

echo Start Application ..........
sleep 1
nohup java -Djava.security.egd=file:/dev/./urandom -jar -server -Xms400m -Xmx400m -Xmn120M   -Xss256k -XX:PermSize=32m -XX:MaxPermSize=32m  -XX:+PrintGCDetails  -XX:+PrintGCTimeStamps -XX:+HeapDumpOnOutOfMemoryError  admin.jar --spring.profiles.active=dev  >/dev/null 2>&1
sleep 5
