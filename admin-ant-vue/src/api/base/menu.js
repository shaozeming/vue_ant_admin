import request from '../../common/request'

const base="adminApi/permission";


export  function selectMenuList(param) {
    return request({
        url: base+'/selectMenuList',
        method: 'post',
        data: {...param, crypto:true}
    })
}


export  function selectById(param) {
    return request({
        url: base+'/selectById',
        method: 'post',
        data: {...param, crypto:true}
    })
}

export  function save(param) {
    return request({
        url: base+'/save',
        method: 'post',
        data: {...param, crypto:true}
    })
}
export  function deleteById(param) {
    return request({
        url: base+'/deleteById',
        method: 'post',
        data: {...param, crypto:true}
    })
}



