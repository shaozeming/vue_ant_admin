// eslint-disable-next-line
import * as loginService from '@/api/login'
// eslint-disable-next-line
import { BasicLayout, BlankLayout, PageView, RouteView } from '@/layouts'



// 前端路由表
const constantRouterComponents = {
  // 基础页面 layout 必须引入
  BasicLayout: BasicLayout,
  BlankLayout: BlankLayout,
  RouteView: RouteView,
  PageView: PageView,
  '403': () => import(/* webpackChunkName: "error" */ '@/views/exception/403'),
  '404': () => import(/* webpackChunkName: "error" */ '@/views/exception/404'),
  '500': () => import(/* webpackChunkName: "error" */ '@/views/exception/500'),

  // 你需要动态引入的页面组件
  'welcome': () => import('@/views/base/welcome/welcome'),
  'admin': () => import('@/views/base/admin/admin'),

  'syslog': () => import('@/views/base/syslog/syslog'),
  'quartz': () => import('@/views/base/quartz/quartz'),
  'role': () => import('@/views/base/role/role'),
  'menus': () => import('@/views/base/permission/permission'),


  // result
  'ResultSuccess': () => import(/* webpackChunkName: "result" */ '@/views/result/Success'),
  'ResultFail': () => import(/* webpackChunkName: "result" */ '@/views/result/Error'),

  // exception
  'Exception403': () => import(/* webpackChunkName: "fail" */ '@/views/exception/403'),
  'Exception404': () => import(/* webpackChunkName: "fail" */ '@/views/exception/404'),
  'Exception500': () => import(/* webpackChunkName: "fail" */ '@/views/exception/500'),

}

// 前端未找到页面路由（固定不用改）
const notFoundRouter = {
  path: '*', redirect: '/404', hidden: true
}

const index={
  id: -2,
  key: 'welcome',
  name: 'welcome',
  routerUrl: '/',
  parentId: 0,
  componentName: 'welcome',
  menuName:"首页",
  children: [],
  menuIcon: "home"
}

// 根级菜单
const rootRouter = {
  id: -1,
  key: '',
  name: 'index',
  routerUrl: '/',
  componentName: 'BasicLayout',
  redirect: '/welcome',
  children: [],

}

/**
 * 动态生成菜单
 * @param data
 * @returns {Promise<Router>}
 */
export const generatorDynamicRouter = (data) => {



  return new Promise((resolve, reject) => {
    data=[index,...data]

    const menuNav = []
    const childrenNav = []
    //      后端数据, 根级树数组,  根级 PID
    listToTree(data, childrenNav, 0)
    rootRouter.children = childrenNav
    menuNav.push(rootRouter)

    const routers = generator(menuNav)
    routers.push(notFoundRouter)

    // console.log('menuNav', menuNav)
    // console.log('routers', routers)
    resolve(routers)
  })
}

/**
 * 格式化树形结构数据 生成 vue-router 层级路由表
 *
 * @param routerMap
 * @param parent
 * @returns {*}
 */
export const generator = (routerMap, parent) => {
  return routerMap.map(item => {


    const currentRouter = {
      // 如果路由设置了 path，则作为默认 path，否则 路由地址 动态拼接生成如 /dashboard/workplace
      path: item.routerUrl || `${parent && parent.routerUrl || ''}/${item.componentName}`,
      // 路由名称，建议唯一
      name: item.componentName || item.key || '',
      // 该路由对应页面的 组件 :方案1
      // component: constantRouterComponents[item.component || item.key],
      // 该路由对应页面的 组件 :方案2 (动态加载)
      component: (constantRouterComponents[item.componentName || item.key]) || (() => import(`@/views/${item.componentName}`)),

      // meta: 页面标题, 菜单图标, 页面权限(供指令权限用，可去掉)
      meta: {
        title: item.menuName,
        icon: item.menuIcon || undefined,
        hiddenHeaderContent: item.hiddenHeaderContent,
        target: item.target,
        permission: item.componentName
      }
    }


    if(item.menu&&item.children&&item.children.length>0){
      currentRouter.component=constantRouterComponents["RouteView"];
    }

    // 是否设置了隐藏菜单
    if (item.show === false) {
      currentRouter.hidden = true
    }
    // 是否设置了隐藏子菜单
    if (item.hideChildren) {
      currentRouter.hideChildrenInMenu = true
    }
    // 为了防止出现后端返回结果不规范，处理有可能出现拼接出两个 反斜杠
    if (!currentRouter.path.startsWith('http')) {
      currentRouter.path = currentRouter.path.replace('//', '/')
    }
    // 重定向
    item.redirect && (currentRouter.redirect = item.redirect)
    // 是否有子菜单，并递归处理
    if (item.children && item.children.length > 0) {
      // Recursion
      currentRouter.children = generator(item.children, currentRouter)
    }


    return currentRouter
  })
}

/**
 * 数组转树形结构
 * @param list 源数组
 * @param tree 树
 * @param parentId 父ID
 */
const listToTree = (list, tree, parentId) => {

  list.forEach(item => {
    if(!item.children){
      return
    }
    /*按钮去除*/
    if(item.type===2){
      return
    }
    item.menu=true;
    // 判断是否为父级菜单
    if (item.parentId === parentId) {
      const child = {
        ...item,
        key: item.id || item.menuName,
        children: []
      }
      // 迭代 list， 找到当前菜单相符合的所有子菜单
      listToTree(item.children, child.children, item.id)
      // 删掉不存在 children 值的属性
      if (child.children.length <= 0) {
        delete child.children
      }
      // 加入到树中
      tree.push(child)
    }
  })
}
