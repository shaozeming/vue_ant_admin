export default {
  data() {
    return {
      advanced: false,
      selectedRowKeys: [],
      selectedRows: [],
      rowSelection: {
        fixed: true,
        onChange: this.onSelectChange
      },
      tableData: [],
      pagination: {
        current: 1,
        pageSize: 10,
        pageSizeOptions: [
          '10', '20', '30', '40'
        ],
        showQuickJumper: true,
        showSizeChanger: true,
        showTotal: (total) => `共${total}条数据`,
        total: 0,
        change: this.pageChange
      },
      searchParam: {},
      filterParams: {},
      loading: false,
      pageParam: {
        sortParam: '',
        direction: ''
      }

    }
  },
  created() {
    if (!this.noCurdInit) {
      this.initList()
    }

  },
  methods: {
    initList() {
      this.pagination.current = 1
      if (this.preList) {
        this.preList()
      }
      if (this.customerInit) {
        this.customerInit()
      }
      this.list()
    },
    toggleAdvanced() {
      this.advanced = !this.advanced
    },
    search() {
      this.pagination.current = 1
      if (this.searchFilter) {
        this.searchFilter()
      }
      this.list()
    },
    onSelectChange(selectedRowKeys, selectedRows) {
      this.selectedRowKeys = selectedRowKeys
      this.selectedRows = selectedRows
    },
    tableChange(page, filters, sorter) {

      this.pagination.current = page.current
      this.pagination.pageSize = page.pageSize

      this.filterParams={};
      Object.keys(filters).forEach(item => {
        if (filters[item] && filters[item].length > 0) {
          this.filterParams[item] = filters[item].join('||')
        } else {
          delete this.filterParams[item]
        }
      })


      if (sorter.columnKey) {
        let prop = sorter.columnKey
        let pre = ''
        if (prop.indexOf('.') > 0) {
          pre = prop.substring(0, prop.indexOf('.'))
          prop = prop.substring(prop.indexOf('.'), prop.length)
        }
        prop = this.$objUtils.objectTotable(prop)
        if (pre) {
          prop = pre + prop
        }
        this.pageParam.sortParam = prop
        this.pageParam.direction = (sorter.order.startsWith('descend') ? '0' : '1')

      } else {
        this.pageParam.sortParam = ''
        this.pageParam.direction = ''
      }
      this.list()

    },
    reset() {
      this.searchParam = {}
      this.list()
    },
    refresh() {
      if (this.customerInit) {
        this.customerInit()
      }
      this.list()
    },
    list() {

      if (!this.getList) {
        return
      }
      this.loading = true
      this.$nextTick(() => {
        let param = this.getParam()
        this.getList(param).then((res) => {
          if (res.status === 0) {
            this.tableData = res.data.records
            if (res.data.total) {
              this.pagination.total = res.data.total
            }
          } else {
            this.$message.error(res.msg)
          }
          if (this.listBack) {
            this.listBack()
          }
          this.loading = false
        }).catch(res => {
          this.loading = false
        })
      })
    },

    getParam() {

      return {
        page: this.pagination.current,
        size: this.pagination.pageSize,
        ...this.pageParam,
        param: JSON.stringify({ ...this.searchParam, ...this.filterParams })
      }
    },
    add() {
      this.$refs.save.setData()
    },

    handleEdit(data) {
      this.$refs.save.edit({ ...data })
    },
    handleDelete(data) {
      if (this.delete) {
        this.delete(data).then(res => {
          if (res.status === 0) {
            this.$message.success('删除成功')
            this.refresh()
          } else {
            this.$message.error(res.msg)
          }
        })
      }
    },
    handleMenuClick(e) {

      if (!this.multiOperator) {
        return
      }
      let operator = this.multiOperator.find(item => item.key === e.key)
      if (!operator || !operator.method) {
        return
      }

      let ids = this.selectedRowKeys.join(',')
      this.$confirm({
        title: operator.tip || '确定批量操作吗？',
        okText: '确定',
        okType: 'primary',
        cancelText: '取消',
        onOk: () => {

          operator.method({ ids: ids }).then(res => {
            if (res.status === 0) {
              this.$message.success(operator.msg || '批量操作成功!')
              this.refresh()
            } else {
              this.$message.error(res.msg)
            }
          })
        },
        onCancel() {
        }
      })

    }


  }
}
