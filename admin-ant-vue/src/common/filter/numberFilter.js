import Vue from 'vue';



Vue.filter('getMoney', function (value) {
    if(!value){
        return "0.00";
    }
    return parseFloat(value).toFixed(2);
});
