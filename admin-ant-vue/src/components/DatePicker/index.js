import {DatePickerProps} from 'ant-design-vue/es/date-picker/interface'
import moment from 'moment'
import 'moment/locale/zh-cn'

moment.locale('zh-cn')

export default {
  model: {
    prop: 'value',
    event: 'change'
  },
  data() {

    let date = null;
    if (this.value ) {
      if (typeof this.value === 'string') {
        date = moment(this.value, this.format || 'YYYY-MM-DD HH:mm:ss')
      } else {
        date = moment(this.value)
      }
    }
    return {
      date: date
    }
  },
  props: Object.assign({}, DatePickerProps(), {
    value: {
      type: Array
    }
  }),
  watch: {
    value: {
      handler: function(val = []) {
        if (val) {
          if (typeof val === 'string') {
            this.date = moment(val, this.format || 'YYYY-MM-DD HH:mm:ss')
          } else {
            this.date = moment(val)
          }
        }else {
          this.date=null
        }
      },
      immediate: true
    }
  },
  created() {

  },
  methods: {
    change(date, dateString) {

      this.$emit('change',  date.toDate())
    }
  },

  render() {
    const props = {}

    Object.keys(DatePickerProps()).forEach(key => {
      if (this[key]) {
        props[key] = this[key]
      }
    })
    delete props.value
    return (
      <a-date-picker {...{ props, scopedSlots: { ...this.$scopedSlots } }} onChange={this.change} value={this.date}>
        {Object.keys(this.$slots).map(name => (<template slot={name}>{this.$slots[name]}</template>))}
      </a-date-picker>
    )
  }
}
